# Angular Directives Tutorial Session

## Step 1: 

clone the angular seed project 

perform bower install

```
git clone https://github.com/angular/angular-seed.git
bower install
```

OR

```
git checkout -f 4d7580c
```

## Step 2:
remove the footer from view
```
git checkout -f 8706045
```

## Step 3:
Use compile
```
git checkout -f 7825972
```

## Step 4:
Use scope to maintain different scopes
```
git checkout -f c879ad3
```