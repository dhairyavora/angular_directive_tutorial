'use strict';

/* Directives */

angular.module('myApp.directives', [])
    .directive('appVersion', ['version',
        function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }
    ])
    .directive('dirOne', 
        function($compile){
            return {
            /*
            restrict : "A",
            link : function(scope, element, attributes, controller) {
                var markup = "<input type='text' ng-model='sampleData' />{{sampleData}} </br>";
                element.html(markup);
                $compile(element.contents())(scope);
            },*/
            /*restrict : 'E',
            template : "<input type='text' ng-model='sampleData' /> {{sampleData}} </br>",*/
            /*restrict : 'C',
            template : "<input type='text' ng-model='sampleData' /> {{sampleData}} </br>",
            scope : {}*/
           restrict : 'E',
           templateUrl : 'partials/directives/dir1.html',
           scope : {
               alertfn : "&",
               msg : "="
           }
           
        };
    }
);
