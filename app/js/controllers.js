'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('MyCtrl1', ['$scope',
        function($scope) {
        
        }])
    .controller('MyCtrl2', ['$scope',
        function($scope) {
        
        }])
    .controller('SampleDirectiveCtrl', ['$scope',
        function($scope) {
            $scope.testmessage = "Hello World";
            $scope.showAlert = function() {
                alert($scope.testmessage);
            };
    }]); 